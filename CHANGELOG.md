# Revision history for Difluoroborane

## 0.4.3 -- 2025-02-11

* Adds binary version of FabFire authenitcation protocol
* Adds commands to dump and restore the full database as a TOML text file (`--dump-db` and `--load-db`)
* allows compilation with current stable Rust (1.84)
 - Attention: The database format still relies on Rust data layout, so when updating the compiler, the database must be transfered as TOML dump.
   Therefore, the `rust-toolchain.toml` file pinning `rustc` to version `1.66` is still in place.
* resolves a crash (use after free) when disconnecting a client.
* resolves some compiler warnings

## 0.4.2 -- TODO

## 0.4.1 -- 2022-04-24

* Initial full implementation of the FabAccess 0.3 API, "Spigots of Berlin".

## 0.3.0 -- 2021-10-01

* A version seen by enough people that the version number needs to be skipped but never a formally released version

## 0.2.0 -- 2021-02-23

* Dammit, missed by four days.
* First (released) version that actually does something.
* More extensive documentation to follow for 0.2.1ff

## 0.1.0 -- 2020-02-19

* First version. Released on an unsuspecting world.
